orphan-sysvinit-scripts for Debian
---------------------------------

The best place for System-V-like init scripts is in the package
containing the executible which they control.

However, some maintainers no longer want to have them there, which
creates a problem for users of init systems that cannot use systemd
unit files. This package provides a solution to that problem - it
contains (hopefully) all these "orphaned" init scripts, and arranges
that those scripts that correspond to installed packages are placed in
/etc/init.d.

It does this using ucf, which means that the files in /etc/init.d are
treated like configuration files (so preserving user modifications in
the usual manner). dpkg triggers are used to detect when a relevant
package is installed, so you should not need to manage these files
yourself.

If you purge this package it will endeavour to only remove scripts
from /etc/init.d that it has previously managed.

If you discover other orphaned init scripts that should be in this
package, please file a bug report - ideally with a copy of the script
and details of its copyright status.

 -- Matthew Vernon <matthew@debian.org>, Wed,  6 Jan 2021 17:06:03 +0000
